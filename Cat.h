#pragma once

#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "texture_loader.h"
#include "shader_setup.h"
#include <stack>

class Circle;
class Triangle;

using namespace std;

class Cat
{
	Circle *catCircle;
	Triangle *catOuterEar;

	//Shader for the cat
	GLuint catShader;

	//Uniform variable locations in the catShader
	GLuint locT;
	GLuint locViewTransform;

	stack<CoreStructures::GUMatrix4> matrixStack;

public:
	Cat();

	//Render cat object. x position of root, y position of root, size of the cat, angle of the cat(in degrees), viewing transformation
	void renderCat(float x, float y, float scale, float orientation, const CoreStructures::GUMatrix4& viewTransform, CoreStructures::GUMatrix4& R);
};