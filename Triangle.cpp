#include "stdafx.h"
#include "Triangle.h"

using namespace std;
using namespace CoreStructures;

Triangle::Triangle(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
{

	////Setup triangle data
	//static GLfloat triangleVertices[] =
	//{
	//	0.0f, 0.0f,
	//	0.25f, 0.5f,
	//	0.5f, 0.0f
	//};

	//static GLubyte triangleColorData[] =
	//{
	//	r, g, b, a,
	//	r, g, b, a,
	//	r, g, b, a
	//};

	//static GLfloat triangleTexCoords[] =
	//{
	//	0.0f, 0.0f,
	//	0.5f, 1.0f,
	//	1.0f, 0.0f
	//};

	struct TriangleModel
	{
		GLfloat x, y;
		GLubyte r, g, b, a;
		GLfloat u, v;
	};

	static TriangleModel interleavedTriangle[] =
	{
		{ 0.0f, 0.0f, r, g, b, a, 0.0f, 0.0f },
		{ 0.25f, 0.5f, r, g, b, a, 0.5f, 1.0f },
		{ 0.5f, 0.0f, r, g, b, a, 1.0f, 0.0f }
	};

	//Setup VAO
	glGenVertexArrays(1, &triangleVAO);
	glBindVertexArray(triangleVAO);

	//Copy vertex position data to VBO
	glGenBuffers(1, &vertexPositionVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TriangleModel), interleavedTriangle, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(TriangleModel), (const GLvoid*)0);

	//Copy color data to VBO
	glGenBuffers(1, &colorVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TriangleModel), interleavedTriangle, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(TriangleModel), (const GLvoid*)(sizeof(GLubyte)* 4));

	//Copy vertex position data to VBO
	glGenBuffers(1, &texCoordVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TriangleModel), interleavedTriangle, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(TriangleModel), (const GLvoid*)(sizeof(GLubyte)* 2));

	//Enable position and texture coordinate buffer inputs
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);
}

void Triangle::render(void)
{
	glBindVertexArray(triangleVAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}