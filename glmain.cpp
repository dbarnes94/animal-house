//Darrell Barnes
//14029375
//Coursework 1

//Animal House
#include "stdafx.h"
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "texture_loader.h"
#include "shader_setup.h"
#include "Cat.h"
#include <iostream>
#include <wincodec.h>

using namespace std;
using namespace CoreStructures;

GLuint myShaderProgram;
GLuint locT, locViewTransform;

//Window Panes
GLuint paneVAO, paneVerticesVBO, paneColorsVBO, paneTexCoordVBO, paneVertexIndicesVBO;
static GLfloat paneVertices[] =
{
	-0.69f, -0.05f,
	-0.69f, 0.40f,
	-0.30f, 0.40f,
	-0.30f, -0.05f
};

static GLubyte paneColors[] = 
{
	172, 216, 230, 10,
	172, 216, 230, 10,
	172, 216, 230, 10,
	172, 216, 230, 10,
};

static GLfloat paneTexCoords[] =
{
	0.5f, 1.0f,
	0.6f, 0.7f,
	1.0f, 0.7f,
	0.8f, 0.4f,
};

static GLubyte paneVertexIndices[] = { 0, 1, 2, 3, 4};


////Window Cross Section
GLuint crossVerticesVBO, crossColorsVBO;

static GLfloat crossVertices[] =
{
	-0.69f, -0.05f,
	-0.69f, 0.40f,
	-0.30f, 0.40f,
	-0.30f, -0.05f
};

static GLubyte crossColors[] =
{
	52, 10, 0, 255,
	52, 10, 0, 255,
	52, 10, 0, 255,
	52, 10, 0, 255
};

//struct VertexModel 
//{
//	GLfloat			x, y; // Position
//	GLubyte			r, g, b; // Colour
//};
//
//static VertexModel interleavedCross[] = 
//{
//	{ 0.5f, 0.70f, 51, 25, 0 },
//	{ 0.10f, 0.30f, 51, 25, 0 },
//	{ 0.25f, 0.08f, 51, 25, 0 },
//	{ 0.15f, -0.05f, 51, 25, 0 }
//};

//Walls
GLuint wallVAO, wallVerticesVBO, wallColorsVBO;
static GLfloat wallVertices[] =
{
	-0.69f, -0.05f,
	-0.69f, 0.40f,
	-0.30f, 0.40f,
	-0.30f, -0.05f
};

static GLubyte wallColors[] =
{
	172, 216, 230, 255,
	172, 216, 230, 255,
	172, 216, 230, 255,
	172, 216, 230, 255,
};


//Globals
//Variable we'll use to animate (rotate) objects
float theta = 0.0f;

//Cat Model
Cat *myCat = nullptr;

float catX = 0.0f;
float catY = 0.0f;
float catOrientation = 0.0f;

//Camera Model
float cameraX = 0.0f;

//Function Prototypes
void init(int argc, char* argv[]);
void display(void);
void keyDown(unsigned char key, int x, int y);
void keyDownSpec(int key, int x, int y);

void drawWindowFrame();

void setupWindowPanes_VAO(void);
void drawWindowPanes_VAO(void);

void setupCrossVBO();
void drawCrossVBO();

void setupWallVAO();
void drawWallVAO();

int _tmain(int argc, char* argv[]) 
{

	init(argc, argv);

	glutMainLoop();

	//Shut down COM
	shutdownCOM();

	return 0;
}

void init(int argc, char* argv[]) 
{
	//Initialise COM so we can use Windows Imaging Component
	initCOM();

	//Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 4);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("Animal House");

	//Register callback functions
	glutDisplayFunc(display);
	glutKeyboardFunc(keyDown); //Keyboard event handler
	glutSpecialFunc(keyDownSpec); //Keyboard Special event handler

	//Initialise GLEW library
	GLenum err = glewInit();

	//Ensure GLEW was initialised successfully before proceeding
	if (err == GLEW_OK) {

		cout << "GLEW initialised okay\n";
	}
	else {

		cout << "GLEW could not be initialised\n";
		throw;
	}

	// Report maximum number of vertex attributes
	GLint numAttributeSlots;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &numAttributeSlots);
	cout << "GL_MAX_VERTEX_ATTRIBS = " << numAttributeSlots << endl;

	//Example query OpenGL state (get version number)
	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";

	//Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	myShaderProgram = setupShaders(string("Shaders\\basic_vertex_shader.txt"), string("Shaders\\basic_fragment_shader.txt"));

	// Get uniform location of "T" variable in shader program (we'll use this in the display function to give the uniform variable "T" a value)
	locT = glGetUniformLocation(myShaderProgram, "T");
	locViewTransform = glGetUniformLocation(myShaderProgram, "viewTransform");

	// Create new Cat instance
	myCat = new Cat();

	setupWindowPanes_VAO();
	setupWallVAO();
}

void setupWindowPanes_VAO(void)
{
	glGenVertexArrays(1, &paneVAO);
	glBindVertexArray(paneVAO);

	glGenBuffers(1, &paneVerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, paneVerticesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(paneVertices), paneVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &paneColorsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, paneColorsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(paneColors), paneColors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &paneTexCoordVBO);
	glBindBuffer(GL_ARRAY_BUFFER, paneTexCoordVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(paneTexCoords), paneTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &paneVertexIndicesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, paneVertexIndicesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(paneVertexIndices), paneVertexIndices, GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void setupWallVAO(void)
{
	glGenVertexArrays(1, &wallVAO);
	glBindVertexArray(wallVAO);

	glGenBuffers(1, &wallVerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, wallVerticesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), wallVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &wallColorsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, wallColorsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wallColors), wallColors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
}

void setupCrossVBO()
{
	glGenBuffers(1, &crossVerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, crossVerticesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(crossVertices), crossVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &crossColorsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, paneColorsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(crossColors), crossColors, GL_STATIC_DRAW);
}

//Rendering
void display(void) 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(myShaderProgram);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glPolygonMode(GL_FRONT, GL_LINE);

	//Window Pane Bottom Left
	GUMatrix4 T = GUMatrix4::translationMatrix(0.0f, 0.0f, 0.0f);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);

	GUMatrix4 cameraViewTransform = GUMatrix4::translationMatrix(cameraX, 0.0f, 0.0f);
	//Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&cameraViewTransform);

	drawWindowPanes_VAO();

	//Window Pane Top Left
	T = GUMatrix4::translationMatrix(0.0f, 0.60f, 0.0f);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);

	cameraViewTransform = GUMatrix4::translationMatrix(cameraX, 0.0f, 0.0f);
	//Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&cameraViewTransform);

	drawWindowPanes_VAO();

	//Window Pane Top Right
	T = GUMatrix4::translationMatrix(0.49f, 0.60f, 0.0f);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);

	cameraViewTransform = GUMatrix4::translationMatrix(cameraX, 0.0f, 0.0f);
	//Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&cameraViewTransform);

	drawWindowPanes_VAO();

	//Window Pane Bottom Right
	T = GUMatrix4::translationMatrix(0.49f, 0.0f, 0.0f);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);

	cameraViewTransform = GUMatrix4::translationMatrix(cameraX, 0.0f, 0.0f);
	//Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&cameraViewTransform);

	drawWindowPanes_VAO();

	//Right Wall
	T = GUMatrix4::translationMatrix(0.49f, 0.0f, 0.0f) * GUMatrix4::scaleMatrix(2.0f, 2.0f, 0.0f);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);

	cameraViewTransform = GUMatrix4::translationMatrix(cameraX, 0.0f, 0.0f);
	//Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&cameraViewTransform);

	drawWallVAO();

	glUseProgram(0); //Turn off Shader
	drawWindowFrame();

	GUMatrix4 R = GUMatrix4::identity();
	glLoadMatrixf((GLfloat*)&R);

	myCat->renderCat(catX, catY, 0.1f, 0.0f, cameraViewTransform, R);

	glutSwapBuffers();
}


//Drawing Functions
void drawWindowFrame()
{
	//Bottom Sill
	glBegin(GL_QUADS);
	
	glColor3ub(101, 67, 33);
	glVertex2f(0.25f, -0.17f);
	glVertex2f(0.25f, -0.23f);
	glVertex2f(-0.75f, -0.23f);
	glVertex2f(-0.75f, -0.17f);

	glEnd();

	//Top Sill
	glBegin(GL_QUADS);

	glColor3ub(139, 69, 19);
	glVertex2f(0.25f, -0.17f);
	glVertex2f(0.19f, -0.05f);
	glVertex2f(-0.69f, -0.05f);
	glVertex2f(-0.75f, -0.17f);

	glEnd();

	//Right-Side Wall
	glBegin(GL_QUADS);

	glColor3ub(164, 148, 128);
	glVertex2f(0.25f, -0.17f);
	glVertex2f(0.19f, -0.05f);
	glVertex2f(0.19f, 1.0f);
	glVertex2f(0.25f, 1.0f);

	glEnd();

	//Reft-Side Wall
	glBegin(GL_QUADS);
	
	glColor3ub(164, 148, 128);
	glVertex2f(-0.69f, -0.05f);
	glVertex2f(-0.75f, -0.17f);
	glVertex2f(-0.75f, 1.0f);
	glVertex2f(-0.69f, 1.0f);

	glEnd();
}

//void drawCrossVBO()
//{
//	glBindBuffer(GL_ARRAY_BUFFER, crossVerticesVBO);
//	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
//	glEnableVertexAttribArray(0);
//
//	glBindBuffer(GL_ARRAY_BUFFER, crossColorsVBO);
//	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)0);
//	glEnableVertexAttribArray(1);
//
//	glDrawArrays(GL_QUADS, 0, 4);
//}

void drawWindowPanes_VAO(void)
{
	// Draw the object - same function call as used for vertex arrays but the last parameter is interpreted as an offset into the currently bound index buffer (set to 0 so we start drawing from the beginning of the buffer).
	glBindVertexArray(paneVAO);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, (GLvoid*)0);
}

void drawWallVAO(void)
{
	// Draw the object - same function call as used for vertex arrays but the last parameter is interpreted as an offset into the currently bound index buffer (set to 0 so we start drawing from the beginning of the buffer).
	glBindVertexArray(wallVAO);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, (GLvoid*)0);
}

#pragma region Event handling functions

void keyDown(unsigned char key, int x, int y) 
{
	//Reset the scene
	if (key == 'r')
	{
		cameraX = 0.0f;
		
		glutPostRedisplay();
	}

	//Change time of day
	if (key == 't') 
	{
		glutPostRedisplay();
	}

	if (key == 'w')
	{

	}
}

void keyDownSpec(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT && cameraX <= 1)
	{
		cameraX += 0.0500f;

		glutPostRedisplay();
	}
	else if (key == GLUT_KEY_RIGHT && cameraX >= -0.4)
	{
		cameraX -= 0.0500f;

		glutPostRedisplay();
	}
}
#pragma endregion