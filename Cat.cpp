#include "stdafx.h"
#include "Cat.h"
#include "Circle.h"
#include "Triangle.h"

using namespace std;
using namespace CoreStructures;

Cat::Cat()
{
	catCircle = new Circle(100, 100, 100, 255);
	catOuterEar = new Triangle(100, 100, 100, 255);

	//Load the shaders files for the catShader
	catShader = setupShaders(string("Shaders\\basic_vertex_shader.txt"), string("Shaders\\basic_fragment_shader.txt"));

	//Setup uniform locations
	locT = glGetUniformLocation(catShader, "T");
	locViewTransform = glGetUniformLocation(catShader, "viewTransform");
}

void Cat::renderCat(float x, float y, float scale, float orientation, const CoreStructures::GUMatrix4& viewTransform, CoreStructures::GUMatrix4& R) 
{

	//"Plug in" the cat shader into the GPU
	glUseProgram(catShader);

	// Pass camera transform matrix into 'viewTransform' variable in shader
	glUniformMatrix4fv(locViewTransform, 1, GL_FALSE, (GLfloat*)&viewTransform);

	////Draw the body
	//Create matrices based on input parameters
	//GUMatrix4 bodyTransform = GUMatrix4::translationMatrix(x, y, 0.0f) * GUMatrix4::rotationMatrix(0.0f, 0.0f, orientation * gu_radian) * GUMatrix4::scaleMatrix(scale * 0.65f, scale, 1.0f);

	R = GUMatrix4::translationMatrix(x, y, 0.0f) * GUMatrix4::rotationMatrix(0.0f, 0.0f, orientation * gu_radian) * GUMatrix4::scaleMatrix(scale * 0.65f, scale, 1.0f);

	//Upload body transform to shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	//Draw the circle for the Body
	catCircle->render();

	matrixStack.push(R);

	////Draw the head
	// - Setup the RELATIVE transformation from the centre of the body to where the head's origin will be
	// - Offsets are calculated in terms of the parent object's modelling coordinates.  Any scaling, rotation etc. of the parent will be applied later in the matrix sequence.
	GUMatrix4 body_to_head_transform = GUMatrix4::translationMatrix(-0.20f, 1.20f, 0.0f) * GUMatrix4::scaleMatrix(1.0f, 0.60f, 1.0f);

	// Since we're working with a relative transform, we must multiply this with the parent objects's (that is, the body's) transform also
	// REMEMBER: THE RELATIVE TRANSFORM GOES LAST IN THE MATRIX MULTIPLICATION SO IT HAPPENS FIRST IN THE SEQUENCE OF TRANSFORMATIONS
	R = R * body_to_head_transform;

	// Upload the transform to the shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	// Draw the circle for the head
	catCircle->render();

	matrixStack.push(R);

	//Draw the left outer ear
	GUMatrix4 head_to_left_ear_transform = GUMatrix4::translationMatrix(-0.9f, 0.2f, 0.0f) * GUMatrix4::rotationMatrix(0.0f, 0.0f, 0.7f);

	R = R * head_to_left_ear_transform;

	// Upload the transform to the shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	catOuterEar->render();

	R = matrixStack.top();
	matrixStack.pop();

	matrixStack.push(R);

	//Draw the right outer ear
	GUMatrix4 head_to_right_ear_transform = GUMatrix4::translationMatrix(0.1f, 0.9f, 0.0f) * GUMatrix4::rotationMatrix(0.0f, 0.0f, -0.7f);

	R = R * head_to_right_ear_transform;

	// Upload the transform to the shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	catOuterEar->render();

	R = matrixStack.top();
	matrixStack.pop();

	R = matrixStack.top();
	matrixStack.pop();

	matrixStack.push(R);

	//Draw the first part of the tail
	GUMatrix4 body_to_tailA_transform = GUMatrix4::translationMatrix(0.0f, -1.0f, 0.0f) * GUMatrix4::scaleMatrix(0.25f, 0.4f, 0.0f);

	R = R * body_to_tailA_transform;

	// Upload the transform to the shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	catCircle->render();

	matrixStack.push(R);

	//Draw the second part of the tail
	GUMatrix4 tailA_to_tailB_transform = GUMatrix4::translationMatrix(0.8f, -2.2f, 0.0f) * GUMatrix4::scaleMatrix(1.3f, 2.0f, 0.0f) * GUMatrix4::rotationMatrix(0.0f, 0.0f, 1.4f);

	R = R * tailA_to_tailB_transform;

	// Upload the transform to the shader
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&R);

	catOuterEar->render();

	R = matrixStack.top();
	matrixStack.pop();

	R = matrixStack.top();
	matrixStack.pop();
}