#pragma once

#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "texture_loader.h"
#include "shader_setup.h"


class Ellipses
{
	//Variables to represent the VBO and VAO of the ellipses object
	GLuint ellipsesVAO, vertexPositionVBO, texCoordVBO, colorVBO;

public:

	//Default constructor - setup ellipses with unit radius
	Ellipses();

	//Render ellipses - all relevant transformations are assumed to be setup before calling this function
	void render(void);
};