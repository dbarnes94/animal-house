#include "stdafx.h"
#include "Ellipses.h"

using namespace std;
using namespace CoreStructures;

//Global variable to determine number of points on the ellipses. Since we draw the filled in ellipses with triangle fan the actual number of points stored = num_points_on_ellipses + 2 since we need to store the point at the centre of the ellipses first and the first point on the circumference at the end of the triangle fan point list to close the loop
const int num_points_on_ellipses = 32;
const int num_points = num_points_on_ellipses + 2;

Ellipses::Ellipses() {

	//Setup ellipses data

	//Setup vertex xy coordinates and texture coordinates (modelling coordinates)
	GUVector2 *vertex_xy_coords = (GUVector2*)malloc(num_points * sizeof(GUVector2));
	GUVector2 *vertex_texture_coords = (GUVector2*)malloc(num_points * sizeof(GUVector2));

	static GLubyte colorData[] =
	{
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255,
		100, 100, 100, 255
	};

	if (vertex_xy_coords && vertex_texture_coords)
	{

		// Store point at origin (centre of ellipses / triangle fan used to render the ellipses)
		vertex_xy_coords[0].x = 0.0f;
		vertex_xy_coords[0].y = 0.0f;

		vertex_texture_coords[0].x = 0.5f;
		vertex_texture_coords[0].y = 0.5f;

		// Setup points around the circumference of the ellipses
		float theta = 0.0f;
		float theta_delta = gu_pi * 2.0f / float(num_points_on_ellipses);

		for (int i = 1; i < num_points; ++i, theta += theta_delta)
		{

			float x = cosf(theta);
			float y = sinf(theta);

			// Setup vertex coordinate
			vertex_xy_coords[i].x = 0.8f * x;
			vertex_xy_coords[i].y = y;

			// Setup corresponding texture coordinate
			vertex_texture_coords[i].x = (x * 0.5f) + 0.5f;
			vertex_texture_coords[i].y = 1.0f - ((y * 0.5f) + 0.5f);
		}

		//// Setup VBOs
		glGenVertexArrays(1, &ellipsesVAO);
		glBindVertexArray(ellipsesVAO);

		//Copy vertex position data to VBO
		glGenBuffers(1, &vertexPositionVBO);
		glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
		glBufferData(GL_ARRAY_BUFFER, num_points * sizeof(GUVector2), vertex_xy_coords, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

		//Copy color data to VBO
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(colorData), colorData, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)0);

		//Copy texture coordinate data to VBO
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, num_points * sizeof(GUVector2), vertex_texture_coords, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

		// Enable position and texture coordinate buffer inputs
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glBindVertexArray(0);

		// Once VBOs setup we can dispose of the buffers in system memory - they are no longer needed
		free(vertex_xy_coords);
		free(vertex_texture_coords);
	}
}

void Ellipses::render(void)
{
	glBindVertexArray(ellipsesVAO);
	glDrawArrays(GL_TRIANGLE_FAN, 0, num_points);
}