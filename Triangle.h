#pragma once

#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include "texture_loader.h"
#include "shader_setup.h"

class Triangle
{
	//Variables to represent the VBO and VAO of the triangle object
	GLuint triangleVAO, vertexPositionVBO, texCoordVBO, colorVBO;

public:
	//Default constructor - setup triangle with a color
	Triangle(GLubyte r, GLubyte g, GLubyte b, GLubyte a);

	//Render the triangle - all relevant transformations are assumed to be setup before calling this function
	void render(void);
};